import { Midi } from "@tonejs/midi";
import { execSync } from "child_process";
import { copyFileSync, existsSync, mkdirSync, readFileSync, rmSync, writeFileSync } from "fs";
import { resolve } from "path";
import generateSegment from "./segment";


try { rmSync(resolve('./tmp'), { force: true, recursive: true }); } catch(err) {}
try { mkdirSync(resolve('./tmp')); } catch(err) {}
try { mkdirSync(resolve('./tmp/scad')); } catch(err) {}
copyFileSync(resolve('./src/segment.scad'), resolve('./tmp/scad/segment.scad'));
try { mkdirSync(resolve('./tmp/stl')); } catch(err) {}

let validInput = '';
for(let arg of process.argv) {
  if(existsSync(resolve(arg)) && arg.includes('mid')) {
    validInput = arg;
  }
}

const overlapLength = 1;
const segmentLength = (6 * 8) + 1;
const firstSegmentLength = segmentLength - 8;
if(validInput.length > 0) {
  const input = new Midi(readFileSync(resolve(validInput)));
  const notes = input.tracks[0].notes;
  if(notes.length > 0){
    const maxLength = Math.ceil(notes[notes.length - 1].ticks / 240);
    const segmentCount = Math.ceil((maxLength - firstSegmentLength) / segmentLength) + 1;
    for(let i = 0;i < segmentCount;i++) {
      console.log(`Generating segment ${i + 1} of ${segmentCount}`);
      let startTick = 0;
      if(i > 0) {
        startTick += firstSegmentLength * 240;
        startTick += (i - 1) * segmentLength * 240;
      }
      let endTick = startTick + ((segmentLength + overlapLength) * 240);
      let fileName = `output${i}.scad`;
      writeFileSync(
        resolve(`./tmp/scad/${fileName}`),
        generateSegment(
          i === 0 ? firstSegmentLength + overlapLength : segmentLength + overlapLength,
          notes.filter(n => n.ticks >= startTick && n.ticks <= endTick),
          startTick,
          i === 0,
          0.4,
          overlapLength
        )
      );
      execSync(`openscad -o ../stl/output${i}.stl ${fileName}`, {
        cwd: resolve('./tmp/scad')
      });
    }
  }
  else {
    console.log('Midi file is missing notes from the first track.');
  }
}
else {
  console.log('No valid midi file specified.');
}