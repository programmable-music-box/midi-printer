import { Note } from "@tonejs/midi/dist/Note";

const DICT: Record<number,number> = {
  88: 0,
  86: 1,
  84: 2,
  83: 3,
  82: 4,
  81: 5,
  80: 6,
  79: 7,
  78: 8,
  77: 9,
  76: 10,
  75: 11,
  74: 12,
  73: 13,
  72: 14,
  71: 15,
  70: 16,
  69: 17,
  68: 18,
  67: 19,
  66: 20,
  65: 21,
  64: 22,
  62: 23,
  60: 24,
  59: 25,
  57: 26,
  55: 27,
  50: 28,
  48: 29
};

function convertNoteX(note: Note): number {
  return DICT[note.midi];
}

export default function generateSegment(length: number, notes: Note[], startTick: number, starting: boolean = false, thickness = 0.4, overlapLength = 2): string {
  let convertedNotes = notes.map(n => [convertNoteX(n),(n.ticks - startTick) / 240]);
  return `include <segment.scad>;
segment(${length},${JSON.stringify(convertedNotes)},${starting},${thickness},${overlapLength});`;
}