module segment(length = 8, notes = [], starting = false, thickness = 0.4, overlap = 2) {
  difference() {
    segmentWidth = 70;
    noteLength = 2.4;
    noteGap = 4;
    noteWidth = 1.9;
    union() {
      //Add starting triangle
      if(starting) {
        translate([0,-4*8,0]) {
          difference() {
            cube([segmentWidth,4*8,thickness]);
            linear_extrude(height = thickness) {
              polygon([[10,0],[segmentWidth,30],[segmentWidth,0]]);
            } 
          }
        }
      }
      cube([70,length*noteGap,thickness]);
    }
    //Add notches for overlap lineup
    if(!starting) {
      translate([1, (overlap*noteGap) - 1, thickness/2]) {
        cube([2,2,thickness], true);
      }
      translate([69, (overlap*noteGap) - 1, thickness/2]) {
        cube([2,2,thickness], true);
      }
    }
    translate([1, (length*noteGap) - (overlap*noteGap) + 1, thickness/2]) {
      cube([2,2,thickness], true);
    }
    translate([69, (length*noteGap) - (overlap*noteGap) + 1, thickness/2]) {
      cube([2,2,thickness], true);
    }
    //Remove half thickness at overlap
    translate([0,(length*noteGap) - (overlap*noteGap),thickness/2]) {
      cube([70,(overlap*noteGap) + 2,thickness/2],false);
    }
    //Add notes
    for(note = notes) {
      translate([6 + (note[0] * 2), (note[1] * noteGap) + (noteLength/2), thickness/2]) {
        cube([noteWidth,noteLength,thickness], true);
      }
    }
  }
}